import { Test4LayoutPage } from './app.po';

describe('test4-layout App', () => {
  let page: Test4LayoutPage;

  beforeEach(() => {
    page = new Test4LayoutPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
