import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app5',
  templateUrl: './app5.component.html',
  styleUrls: ['./app5.component.css']
})
export class App5Component implements OnInit {

  @Input() message: string = "Not set";
  private eventHub: any;
  private eventReceived: boolean = false;
  
  constructor(){
    
  }
  
  setEventHub(hub: any){
    this.eventHub = hub;
    //Register your events here
    
    this.eventHub.on("someEvent", ()=>{
      this.eventReceived = true;
    });
  }

  ngOnInit() {
  }

}
