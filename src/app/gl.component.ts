import {
  Component, ComponentFactoryResolver, HostListener, ComponentFactory, ComponentRef,ViewContainerRef,ReflectiveInjector,
  ElementRef
} from '@angular/core';
import {AppComponent} from './app.component';
import {App2Component} from './app2.component';
import {App4Component} from './app4/app4.component';
import {App5Component} from './app5/app5.component';
import * as $ from 'jquery';
declare var GoldenLayout: any;



@Component({
  selector: 'golden-layout',
  template: `<div style="width:100%;height:100vh;" id="layout"></div>
  <br/><button (click)="sendEvent()">Send event through hub</button>`,
  entryComponents: [AppComponent, App2Component, App4Component, App5Component]
})
export class GLComponent { 
  private config: any;
  private layout: any;
  
  constructor(private el: ElementRef, private viewContainer: ViewContainerRef,
        private componentFactoryResolver: ComponentFactoryResolver){

    this.config = {
            content: [{
                type: 'row',
                content: [{
                    type: 'component',
                    componentName: 'test1',
                    componentState: {
                      message:"Top Left"
                    }
                }, { 
                        type: 'column',
                        content: [{
                            type: 'component',
                            componentName: 'test2',
                            componentState: {
                              message:"Top Right"
                            }
                        }, {
                                type: 'component',
                                componentName: 'test1',
                                componentState: {
                                  message:"Bottom Right"
                                }
                            }, {
                                type: 'component',
                                componentName: 'test4',
                                componentState: {
                                  message:"Bottom Right"
                                }
                            }, {
                                type: 'component',
                                componentName: 'test5',
                                componentState: {
                                  message:"Bottom Right"
                                }
                            }]
                    }]
            }]
        };
  }
  
  ngOnInit(){
    this.layout = new GoldenLayout(this.config, $(this.el.nativeElement).find("#layout"));
    
    this.layout.registerComponent('test1', (container, componentState) => {
          let factory = this.componentFactoryResolver.resolveComponentFactory(AppComponent);
          
          var compRef = this.viewContainer.createComponent(factory);
          compRef.instance.setEventHub(this.layout.eventHub);
          compRef.instance.message = componentState.message;
          container.getElement().append($(compRef.location.nativeElement)); 
    }); 
    
    this.layout.registerComponent('test2', (container, componentState) => {
          let factory = this.componentFactoryResolver.resolveComponentFactory(App2Component);
          
          var compRef = this.viewContainer.createComponent(factory);
          compRef.instance.setEventHub(this.layout.eventHub);
          compRef.instance.message = componentState.message;
          container.getElement().append($(compRef.location.nativeElement)); 
    }); 
    
    this.layout.registerComponent('test4', (container, componentState) => {
          let factory = this.componentFactoryResolver.resolveComponentFactory(App4Component);
          
          var compRef = this.viewContainer.createComponent(factory);
          compRef.instance.setEventHub(this.layout.eventHub);
          compRef.instance.message = componentState.message;
          container.getElement().append($(compRef.location.nativeElement)); 
    }); 

    this.layout.registerComponent('test5', (container, componentState) => {
          let factory = this.componentFactoryResolver.resolveComponentFactory(App5Component);
          
          var compRef = this.viewContainer.createComponent(factory);
          compRef.instance.setEventHub(this.layout.eventHub);
          compRef.instance.message = componentState.message;
          container.getElement().append($(compRef.location.nativeElement)); 
    }); 


    this.layout.init();
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (this.layout)
        this.layout.updateSize();
  }
  
  sendEvent(){
    if (this.layout)
      this.layout.eventHub.emit("someEvent");
  }
}

