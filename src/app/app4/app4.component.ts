import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app4',
  templateUrl: './app4.component.html',
  styleUrls: ['./app4.component.css']
})
export class App4Component implements OnInit {

  @Input() message: string = "Not set";
  private eventHub: any;
  private eventReceived: boolean = false;
  
  constructor(){
    
  }
  
  setEventHub(hub: any){
    this.eventHub = hub;
    //Register your events here
    
    this.eventHub.on("someEvent", ()=>{
      this.eventReceived = true;
    });
  }

  ngOnInit() {
  }

}
