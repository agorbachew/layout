import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { App2Component }   from './app2.component';
import { AppService }   from './app.service';
import { GLComponent }   from './gl.component';

import { AppComponent } from './app.component';
import { App4Component } from './app4/app4.component';
import { App5Component } from './app5/app5.component';

@NgModule({
  declarations: [
    AppComponent, GLComponent, App2Component, App4Component, App5Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [ AppService ],
  bootstrap: [GLComponent]
})
export class AppModule { }


